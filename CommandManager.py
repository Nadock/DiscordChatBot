import logging

import discord

import Commands
import PermissionsManager

class CommandManager():
	"""
		CommandManager passes received messages to the correct command class to be processed.
	"""

	"""
		Creates a new CommandManager to handled commands matches to the supplied commandPrefix.

		@param1 self
		@param2 commandPrefix 	The prefix to a message that denotes it is a command.
		@return A new CommandManager object
	"""
	def __init__(self, commandPrefix):
		self.commandPrefix = commandPrefix
		self.commands = []

		for x in dir(Commands):
			if "Command" in x:
				self.commands.append(getattr(Commands, x)(self.commandPrefix, self))

		self.permissions = PermissionsManager.PermissionsManager()

		self.log = logging.getLogger("CommandManager")
		self.log.debug("Command processing ready")

	"""
		Checks a supplied message to find a matching command and passes the message to that command's command object for processing.

		Runs asynchronously from the event loop and is a coroutine.

		@param1 self
		@param2 message 	The discord.Message that contains all the information relating to the supplied message
	"""
	async def parseCommandMessage(self, message):
		# First check to see if this command matches any aliases
		commandStr = message.content.split()[0]
		for commandClass in self.commands:
			if commandClass.name == "Alias":
				for alias in commandClass.aliases:
					if (self.commandPrefix + alias) == commandStr:
						# If we do find an aliased command, replace it in the message content with the command the alias represents
						found = True
						message.content = self.commandPrefix + commandClass.aliases[alias]
						self.log.info("Alias in message translated from " + alias + " to " + commandClass.aliases[alias])

		# Match the message to a command
		commandStr = message.content.split()[0]
		found = False
		for x in self.commands:
			if commandStr == x.command:
				self.log.info("Treating " + commandStr + " as " + x.name + " command.")
				found = True
				await x.action(self.client, message)

		# Provide the user feedback if they tried to call a command that doesn't exist
		if not found:
			self.log.info("Unable to find matching command for " + commandStr)
			await self.client.send_message(message.channel, "Unable to find matching command for " + commandStr)

	def setClient(self, client):
		self.client = client
