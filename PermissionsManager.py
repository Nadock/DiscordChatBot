import configparser
import logging
from os import path as oPath

import Commands

class PermissionsManager():
	"""
		docstring for PermissionsManager
	"""
	def __init__(self):
		self.log = logging.getLogger("PermissionsManager")

		permissions = configparser.ConfigParser()
		try:
			permissions.read(oPath.abspath("./config/permissions.ini"))
		except IOError as e:
			self.log.error(e)
			raise e

		permissionGroups = []
		for group in permissions.sections():
			newGroup = PermissionGroup(group)
			
			for comm in permissions[group]["commands"].split(sep=" "):
				if comm != "":
					newGroup.addCommand(comm)

			for memb in permissions[group]["members"].split(sep=" "):
				if memb != "":
					newGroup.addMember(int(memb))

			for pcom in permissions[group]["permissionCommands"].split(sep=" "):
				if pcom != "":
					newGroup.addPermissionsCommand(pcom)

			newGroup.setMacroMaxLength(int(permissions[group]["macroMaxLength"]))

			permissionGroups.append(newGroup)

		# for x in permissionGroups:
		# 	print(x.name)
		# 	print("\tCommands:" + str(x.avaliableCommands))
		# 	print("\tMacroMaxLength: "+str(x.macroMaxLength))
		# 	print("\tMembers:" + str(x.memebers))
		# 	print("\tPermissionCommands:" + str(x.permissionCommands))
			

	def checkCommand(self, memberId, commandName):
		inGroup = False
		for x in self.permissionGroups:
			if memberId in x.members:
				group = x
				break

		# If a user is not in a group (this can happen the first time a user uses a command), add them to the STANDARD group
		if not inGroup:
			for x in self.permissionGroups:
				if x.name == "STANDARD":
					x.addMember(memberId)
					group = x
					break

		return (commandName in group.avaliableCommands)

class PermissionGroup(object):
	"""docstring for PermissionGroup"""
	def __init__(self, name):
		self.name = name
		self.avaliableCommands = []
		self.macroMaxLength = 0
		self.memebers = []
		self.permissionCommands = []

	def addCommand(self, commandName):
		if commandName not in self.avaliableCommands:
			self.avaliableCommands.append(commandName)

	def removeCommand(self, commandName):
		self.avaliableCommands[:] = [x for x in self.avaliableCommands if x != commandName]

	def setMacroMaxLength(self, maxLen):
		self.macroMaxLength = maxLen

	def addMember(self, memberId):
		if memberId not in self.memebers:
			self.memebers.append(memberId)

	def removeMember(self, memberId):
		self.memebers[:] = [x for x in self.memebers if x != memberId]

	def addPermissionsCommand(self, commandName):
		if commandName not in self.permissionCommands:
			self.permissionCommands.append(commandName)

	def removePermissionsCommand(self, commandName):
		self.permissionCommands[:] = [x for x in self.permissionCommands if x != commandName]
