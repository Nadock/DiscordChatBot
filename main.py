import asyncio
import configparser
import logging
from os import path as oPath
import selectors
import sys

import discord

import CommandManager

class MessageHandler(discord.Client):
	"""
		MessageHandler overrides several of discord.Client's functions to allow the receipt of message based commands
	"""

	"""
		Creates a new MessageHandler as a child class of discord.Client and stores a reference to the commandManager to use.

		@param1 self
		@param2 commandManager 	The commandManager to send commandMessages to later
		@return A new MessageHandler object
	"""
	def __init__(self, commandManager):
		super(MessageHandler, self).__init__()
		self.commandManager = commandManager
		self.commandManager.setClient(self)
		self.log = logging.getLogger("messageHandler")

	"""
		startMessageHandler start the event loop provided as part of the discord.Client class and handles any errors that arise from doing so.

		@param1 self
		@param2 token 	The Discord App bot token used to login to the Discord servers
		@return none
	"""
	def startMessageHandler(self, token):
		self.log.info("Starting DiscordBot")
		if token == "":
			self.log.CRITICAL("Please supply a bot token in the config.ini file!")
			print("Please supply a bot token in the config.ini file!")
			self.close()
			sys.exit(1)

		try:
			self.run(token)
		except Exception as e:
			self.log.CRITICAL("Login unsuccessful, check bot token and internet connection and please try again")
			print("Login unsuccessful, check bot token and internet connection and please try again")
			raise e
		finally:
			self.close()
			sys.exit(1)

	"""
		Called when the login component of discord.Client.run() completes.

		Runs asynchronously from the event loop and is a coroutine.

		@param1 self
		@return none
	"""
	async def on_ready(self):
		self.log.info("Logged in as <@" + str(self.user.id) + "> " + self.user.name)

	"""
		on_message checks each message to see if it begins with the commandPrefix (defined in config.ini), and if it does the message is passed to the command manager.

		Called whenever the bot receives a message on any Discord server it is connected to.

		Runs asynchronously from the event loop and is a coroutine.

		@param1 self
		@param2 message 	The discord.Message object containing all the information for the received Discord chat message.
		@return none
	"""
	async def on_message(self, message):
		self.log.debug("Received message: \"" + message.content + "\"")

		if message.author.id == self.user.id:
			self.log.debug("Ignoring message from self")
		elif message.content.startswith(self.commandManager.commandPrefix):
			await self.commandManager.parseCommandMessage(message)

"""
	The main function loads the settings in config.ini, creates new a CommandManager and MessageHandler, links the two, and starts the login process via MessageHandler.startMessageHandler(...)

	@return Exit status code
"""
def main():
	config = configparser.ConfigParser()
	config.read(oPath.abspath("./config/config.ini"))

	# Set up logging
	loggers = []
	loggers.append(logging.getLogger("commandManager"))
	loggers.append(logging.getLogger("commands"))
	loggers.append(logging.getLogger("discord"))
	loggers.append(logging.getLogger("messageHandler"))

	# Set logging level
	loggingLevel = config.get("settings", "loggingLevel")
	loggingLevel = loggingLevel.upper()

	# Set up file handler for logging
	fileHandler = logging.FileHandler(filename="chatbot.log", encoding="UTF-8", mode="a")
	fileHandler.setFormatter(logging.Formatter("%(asctime)s [%(levelname)s] %(name)s: %(message)s"))

	for logger in loggers:
		if loggingLevel == "CRITICAL":
			logger.setLevel(logging.CRITICAL)
		elif loggingLevel == "ERROR":
			logger.setLevel(logging.ERROR)
		elif loggingLevel == "WARNING":
			logger.setLevel(logging.WARNING)
		elif loggingLevel == "INFO":
			logger.setLevel(logging.INFO)
		elif loggingLevel == "DEBUG":
			logger.setLevel(logging.DEBUG)
		logger.addHandler(fileHandler)

	# Set up and start message handling
	commandManager = CommandManager.CommandManager(config.get("settings", "commandPrefix"))
	messageHandler = MessageHandler(commandManager)
	messageHandler.startMessageHandler(config.get("settings", "discordBotToken"))

if __name__ == '__main__':
	main()
