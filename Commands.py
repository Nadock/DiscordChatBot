import asyncio
from datetime import datetime
import logging
from os import path as oPath
from urllib import request

import discord

# ==============
# CommandObjects
# ==============
#
# The following class definitions are used by the CommandManager above to process command messages received
# by the discord.Client.
#
# They all have the following things in common:
# 	self.name 							- The human readable name of the command
# 	self.command 						- The exact command string
# 	self.description 					- The human readable description of what the command does
# 	self.additionalParamsDesc 			- A string describing any additional command parameters (optional or required)
#
# Every command must be named according to the following pattern:
# 	class [CommandName]Command():
# For example, the Ping command should be defined as:
# 	class PingCommand():
#
# Additionally, every *Command() class must define the same __init__ function:
# 	__init__(self, commandPrefix, commandManager)
#
# Finally, each *Command() class must also define the following function:
# 	async def action(self, client, message)
# This function performs all the computation necessary to do whatever the command says it does. If any reply messages need to be sent for this message, those are also sent from this function. Once this function completes that message that it was passed is considered processes and will not be passed to it again. This runs asynchronously from the main event loop as a coroutine.
#
# 	Please see the docstring for each command class for a brief description of what it does and how it does it.


class PingCommand():
	"""
		The PingCommand replies with the string "!pong" to the channel from which the message was received.
	"""
	def __init__(self, commandPrefix, commandManager):
		self.name = "Ping"
		self.command = commandPrefix + "ping"
		self.description = "Replies with an in-chat \"pong!\""
		self.additionalParamsDesc = ""
		self.log = logging.getLogger("Commands")
		self.log.debug("Ping command enabled")

	async def action(self, client, message):
		await client.send_message(message.channel, "<@" + message.author.id + "> pong!")

class EchoCommand():
	"""
		The EchoCommand repeats back to the author of a message the message's content, minus the "!echo " sub string. The reply message goes to the channel the message was received on.
	"""
	def __init__(self, commandPrefix, commandManager):
		self.name = "Echo"
		self.command = commandPrefix + "echo"
		self.description = "Replies in-chat with the message it received, minus the \"!echo \""
		self.additionalParamsDesc = ""
		self.log = logging.getLogger("Commands")
		self.log.debug("Echo command enabled")

	async def action(self, client, message):
		await client.send_message(message.channel, message.content[6:])

class NowCommand():
	"""
		The NowCommand replies with a human readable message telling the author of the message what the current time is where the bot is running.
	"""
	def __init__(self, commandPrefix, commandManager):
		self.name = "Now"
		self.command = commandPrefix + "now"
		self.description = "Replies with the current date and time"
		self.additionalParamsDesc = ""
		self.log = logging.getLogger("Commands")
		self.log.debug("Now command enabled")

	async def action(self, client, message):
		currentTime = datetime.now();
		timeString = currentTime.strftime("%H:%M on the ")
		dayString = currentTime.strftime("%d")

		# Remove leading 0 from day number
		if dayString.startswith("0"):
			dayString = dayString.replace("0", "")
		timeString += dayString

		# Append the correct suffix to day number
		if currentTime.strftime("%d") == "01":
			timeString += "st"
		elif currentTime.strftime("%d") == "02":
			timeString += "nd"
		elif currentTime.strftime("%d") == "03":
			timeString += "rd"
		else:
			timeString += "th"

		timeString += currentTime.strftime(" of %B, %Y")

		await client.send_message(message.channel, "<@" + message.author.id + "> the current time is " + timeString)

class InviteCommand():
	"""
		The InviteCommand replies with a link that can be used to invite this bot to another channel.
	"""
	def __init__(self, commandPrefix, commandManager):
		self.name = "Invite"
		self.command = commandPrefix + "invite"
		self.description = "Replies with a link you can use to invite the bot to another server"
		self.additionalParamsDesc = ""
		self.log = logging.getLogger("Commands")
		self.log.debug("Invite command enabled")

	async def action(self, client, message):
		await client.send_message(message.channel, "<@" + message.author.id + "> https://discordapp.com/oauth2/authorize?&client_id=" + str(client.user.id) + "&scope=bot&permissions=470019135")

class HelpCommand():
	"""
		The HelpCommand private messages the author with a human readable description of every command this bot supports.
	"""
	def __init__(self, commandPrefix, commandManager):
		self.name = "Help"
		self.command = commandPrefix + "help"
		self.description = "Messages the sender with this message"
		self.additionalParamsDesc = ""
		self.commandList = commandManager.commands
		self.log = logging.getLogger("Commands")
		self.log.debug("Help command enabled")

	async def action(self, client, message):
		# helpStr = "**Available Commands:**\n"
		# for command in self.commandList:
		# 	helpStr += "**" + command.name + "**" + " " + "*" + command.command + "*"
		# 	if command.additionalParamsDesc != "":
		# 		helpStr += " *" + command.additionalParamsDesc + "*"
		# 	helpStr += "\n\t" + command.description + "\n"

		# await client.send_message(message.author, helpStr)

		helpStrs = []
		helpStrs.append("**Available Commands:**\n")
		helpStrsIdx = 0
		for command in self.commandList:
			helpStr = "**" + command.name + "**" + " *" + command.command + "*"
			if command.additionalParamsDesc != "":
				helpStr += " *" + command.additionalParamsDesc + "*"
			helpStr += "\n\t" + command.description + "\n"

			if len(helpStr) + len(helpStrs[helpStrsIdx]) >= 2000:
				helpStrsIdx += 1
				helpStrs.append("")

			helpStrs[helpStrsIdx] += helpStr

		for x in helpStrs:
			await client.send_message(message.author, x)

class SayCommand():
	"""
		The SayCommand works similarly to the echo command, except Discord's text-to-speech feature is enable with the message.
	"""
	def __init__(self, commandPrefix, commandManager):
		self.name = "Say"
		self.command = commandPrefix + "say"
		self.description = "Replies with everything in the message after \"!say \", with text to speech enabled"
		self.additionalParamsDesc = ""
		self.log = logging.getLogger("Commands")
		self.log.debug("Say command enabled")

	async def action(self, client, message):
		await client.send_message(message.channel, message.content[5:], tts=True)

class AliasCommand():
	"""
		The AliasCommand allows users to define their own commands based off of the pre-existing commands the bot already supports.

		The alias command supports adding and removing aliases as well as listing all currently defined aliases.
	"""
	def __init__(self, commandPrefix, commandManager):
		self.name = "Alias"
		self.command = commandPrefix + "alias"
		self.description = "Manage user defined aliases of commands." + "\n\t*[-l [aliasName]]*\t\t\t\tList all the defined aliases. Optionally if a name is supplied, it describes that alias if it is defined." + "\n\t*[-d aliasName]*\t\t\t\tDelete the alias with the given name. WARNING: This cannot be undone." + "\n\t*[aliasName command]*\tDefines a new alias with the given *aliasName* and *command*"
		self.additionalParamsDesc = "[-l [aliasName...]] [-d aliasName...] [aliasName command]"
		self.log = logging.getLogger("Commands")
		self.commandPrefix = commandPrefix

		# Load any pre-existing aliases from the aliases file
		self.aliases = {}
		if oPath.isfile("./config/aliases"):
			self.log.debug("Loading pre-existing alias file")
			with open(oPath.abspath("./config/aliases")) as f:
				for line in f:
					line = line.replace("\n", "")
					if line != "" and not line.startswith("#"):
						(key, val) = line.split(sep=" ", maxsplit=1)
						self.aliases[key] = val
						self.log.debug("Loaded " + key + " alias")
		else:
			self.log.debug("No previous alias file to load")

		self.preExistingCommands = commandManager.commands
		self.log.debug("Alias command enabled")

	async def action(self, client, message):
		args = message.content.split(sep=" ")
		returnMessage = ""

		# args must have len() >= 2 to have any functional meaning
		if len(args) < 2:
			returnMessage = "Not enough parameters. Usage is:\t\n*" + self.command + "* *" + self.additionalParamsDesc + "*\n"
		else:
			# Check for the list or delete flags
			isList = False
			isDelete = False
			if args[1] == "-l":
				isList = True
			elif args[1] == "-d":
				isDelete = True

			if isList or isDelete:
				if len(args) >= 3:
					for i in range(2, len(args)):
						if args[i] in self.aliases:
							if isList:
								returnMessage += self.commandPrefix + args[i] + " = " + self.commandPrefix + self.aliases[args[i]] + "\n"
							elif isDelete:
								self.aliases.pop(args[i])
								returnMessage += "Alias " + self.commandPrefix + args[i] + " deleted.\n"
								self.log.info("Deleted " + args[i] + " alias")
						else:
							returnMessage += args[i] + " is not a defined alias.\n"
				else:
					if isDelete:
						returnMessage += "Please supply > 0 alias names to delete.\n"
					else:
						#list all aliases
						returnMessage += "Aliases: "
						for key in self.aliases:
							returnMessage += key + " "
						returnMessage += "\n"
			else:
				if len(args) >= 3:
					command = ""
					for i in range(2, len(args)):
						command += args[i] + " "

					if args[1].startswith(self.commandPrefix):
						args[1] = args[1].replace(self.commandPrefix, "")

					nameClash = False
					for pec in self.preExistingCommands:
						if pec.command == self.commandPrefix + args[1]:
							nameClash = True

					if nameClash:
						returnMessage += args[1] + " cannot be used an alias name."
					else:
						self.aliases[args[1]] = command
						returnMessage += self.commandPrefix + args[1] + " defined as " + self.commandPrefix + command + "\n"
						self.log.info("Created " + args[1] + " alias")
				else:
					returnMessage += "Please supply an alias name and a command.\n"

			if isList and returnMessage == "":
				returnMessage = "No aliases defined.\n"

		await client.send_message(message.channel, returnMessage)

		# Update the aliases file to contain the change the user made
		with open(oPath.abspath("./config/aliases"), mode="w") as f:
			f.write("# This is aliases file, it contains all the currently defined aliases.\n# Comments left in this file will be deleted when the bot runs\n\n")
			for key in self.aliases:
				f.write(key + " " + self.aliases[key] + "\n")

class MacroCommand():
	"""
		docstring for MacroCommand
	"""
	def __init__(self, commandPrefix, commandManager):
		self.name = "Macro"
		self.command = commandPrefix + "macro"
		self.description = "Define single aliases that execute multiple commands." + "\n\t*[-l [macroName]]*\t\t\t\tList all the defined macros. Optionally if a name is supplied, it describes that macro if it is defined." + "\n\t*[-d macroName]*\t\t\t\tDelete the macro with the given name. WARNING: This cannot be undone." + "\n\t*[macroName]*\t\t\t\tCreate a new macro with the given name. This starts the interactive macro creation process via a private chat with the message author."
		self.additionalParamsDesc = "[-l [macroName...]] [-d macroName...] [macroName]"
		self.log = logging.getLogger("Commands")
		self.log.debug("Macro command enabled")

	async def action(self, client, message):
		await self.client(message.channel, "Not yet implemented, sorry.")

class SetUsernameCommand():
	"""
		The SetUsernameCommand ...
	"""
	def __init__(self, commandPrefix, commandManager):
		self.name = "Set Username"
		self.command = commandPrefix + "setUsername"
		self.description = "Set the username of the bot."
		self.additionalParamsDesc = "newUsername"
		self.log = logging.getLogger("Commands")
		self.log.debug("Set Username command enabled")

	async def action(self, client, message):
		args = message.content.split(sep=" ", maxsplit=1)

		if len(args) != 2:
			returnMessage = "No username supplied, unable to change username."
		else:
			await client.edit_profile(username=args[1])
			returnMessage = "Username changed to " + args[1]

		await client.send_message(message.channel, returnMessage)

class SetAvatarCommand():
	"""
		The SetAvatarCommand ...
	"""
	def __init__(self, commandPrefix, commandManager):
		self.name = "Set Avatar"
		self.command = commandPrefix + "setAvatar"
		self.description = "Set the avatar of the bot.\n\t*[urlToImage]*\t\t\t\tA URL to a JPEG or PNG image. Alternatively, leave blank to clear the avatar (ie: set no avatar)."
		self.additionalParamsDesc = "[urlToImage]"
		self.log = logging.getLogger("Commands")
		self.log.debug("Set Avatar command enabled")

	async def action(self, client, message):
		args = message.content.split(sep=" ", maxsplit=1)

		if len(args) != 2:
			await client.edit_profile(avatar=None)
			returnMessage = "Avatar cleared!"
		else:
			imgRequest = request.urlopen(args[1])
			if imgRequest.getcode() != 200:
				# HTTP error
				returnMessage = "HTTP" + imgRequest.getcode + " error."
			else:
				imgContent = imgRequest.read()
				imgRequest.close()
				try:
					await client.edit_profile(avatar=imgContent)
					returnMessage = "Avatar updated!"
				except InvalidArguemtn as e:
					returnMessage = "Invalid image supplied, images **must** be either JPEG or PNG."

		await client.send_message(message.channel, returnMessage)

class PermissionsCommand():
	"""
		The PermissionsCommand ...
	"""
	def __init__(self, commandPrefix, commandManager):
		self.name = "Permissions"
		self.command = commandPrefix + "permissions"
		self.description = "List and edit user permissions.\n" + "\t*<username | id>*\t\t\t\tList the permissions of the username or id supplied. Optionally, if no name is supplied it list's the message author's permissions.\n" + "\t*[-m [username | id] [groupName]]*\t\t\t\tMove the given username or id to the given permissions group.\n" + "\t*[-c [groupName]]*\t\t\t\tCreate a new permissions group with the given name.\n" + "\t*[-e [groupName] [attr] <+| ->[value]]*\t\t\t\tEdit the permission attribute given in the given group.\n" + "\t\t*+[value]*: Add value to attr list.\n" + "\t\t*-[value]*: Remove value from attr list.\n" + "\t\t *[value]*: Set attr to given value.\n" + "\t*[-r [groupName]]*\t\t\t\tRemove a permissions group and move all previous members to the DEFAULT group."
		self.additionalParamsDesc = "[<username | id ...> | [-m [username | id] [groupName]]] [-c [groupName]] [-e [groupName] [attr] <+ | ->[value]] [-r [groupName]]]"
		self.log = logging.getLogger("Commands")
		self.log.debug("Permissions command enabled")

	async def action(self, client, message):
		pass
