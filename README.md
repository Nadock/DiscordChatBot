# Discord Chat Bot

A simple chat bot for discord written using the discord-py API wrapper, found [here.](https://github.com/Rapptz/discord.py)

## Important Note

Please note that this bot is currently under active development. As such, the current version may or may not run and may have undocumented bugs. Additionally, it should not be considered "feature complete" while this message remains.
If you do use the bot while it is in active development and you have feedback please raise an issue on the github page.

## Requirements
 * Python >= 3.5
 * discord-py 
 
## Installing
Currently, you will need to install discord-py yourself. A later version should bundle this together in an installation script or via pip.

To install discord-py, follow their instructions [here](https://github.com/Rapptz/discord.py#installing)

You can check which version of Python you are using with the following command:
	
	python --version

It should output something like:

	Python 3.5.2

Then to get a copy of the chat bot, clone it with the following command

	git clone https://github.com/Riley1994/DiscordChatBot.git

## Running the Chat Bot

### Getting a Discord App & Bot Token

If you don't have a bot token, you will need one to make the bot work. Follow the steps below to get a bot token:

1. You will first need to create a Discord Application for your bot, to do this go to [the discord developers application page](https://discordapp.com/developers/applications/me) and click "New Application".
2. Fill in the "APP NAME" section with whatever you want to call your application, it doesn't really matter other than for asthetics and can be changed later, then click "Create Application"
3. On the next screen click "Create a Bot User" and "Yes, I'll do it!"
4. Finally, click the "click to revel" link next to the "Token" subheading, this will reveal your Discord App's bot token.

### First Time Setup

To set up the discord bot you will first need to copy the `config-example.ini` file to `config.ini` and then edit the line
	
	discordBotToken:

and add your Discord App's bot token from the steps above just after the ": "

### Actually Running the Bot

From the command line, run the chatbot.py file like you would any normal python program.
	
	> python chatbot.py

or if you have multiple versions of python installed on your system
	
	> python35 chatbot.py

### Adding Your New Bot To It's First Channel

The bot can create this link for you with the !invite command. However, for the first time you wont be able to send it messages yet so use the URL template below, replacing [YOUR BOT'S CLIENT ID] with the Client ID found on your Discord Application page, under the heading App Details.

    https://discordapp.com/oauth2/authorize?&client_id=[YOUR BOT'S CLIENT ID]&scope=bot&permissions=470019135

## Currently Available Commands

To get a more detailed description of each command, try the !help command.

Command | Description
--------|---------
`!ping` | Replies in chat with "pong". Used to check if the chat bot is alive.
`!now` | Replies in chat with the current date and time.
`!invite` | Replies in chat with a link to invite the bot to one of your servers.
`!help` | Private messages the sender with a list of all the possible commands and a description of what they do
`!echo` | Replies with everything in the message after "!echo "
`!say` | Replies with everything in the message after "!say ", with text to speech enabled
`!alias` | Manage user defined aliases of commands.
`!setUsername` | Change the bot's username.
`!setAvatar` | Change the bot's avatar image.
